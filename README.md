Unofficial Raspberry Pi armhf docker build of Certbot
--


This is an unofficial Raspberry Pi docker build of Certbot because the official one does not work on the Raspberry Pi's.

This image is based on resin/raspberry-pi-alpine (with tag 3.7 currently), and certbot version 0.19.0.

See the official certbot docker for full instructions and usage -
https://hub.docker.com/r/certbot/certbot/

Two volume mount points are available -

`/etc/letsencrypt` - All files related to the certificates

`/data/letsencrypt` - Useful for specifying as the target for `--webroot-path` as a shared volume with your web server. This path is used for certbot challenge.

Example Usage -
```sh
$ docker run -it --rm -v certs:/etc/letsencrypt -v certs-data:/data/letsencrypt flyingchipmunk/rpi-certbot certonly --webroot --webroot-path=/data/letsencrypt -d www.yourdomain.com
```

[This repo on docker hub](https://hub.docker.com/r/flyingchipmunk/rpi-certbot/)