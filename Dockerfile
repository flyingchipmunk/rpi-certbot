FROM resin/raspberry-pi-alpine:3.7

LABEL description="Unofficial Certbot build for Raspberry Pi" \
      version="1.0" \
      maintainer="Matthew Veno <matt@flyingchipmunk.com>"

# RUN [ "cross-build-start" ]

RUN apk --no-cache add \
    py2-future \
    py2-certifi \
    py2-urllib3 \
    py2-chardet \
    certbot

# RUN [ "cross-build-end" ]

VOLUME /etc/letsencrypt /data/letsencrypt

EXPOSE 80 443

ENTRYPOINT [ "certbot" ]
